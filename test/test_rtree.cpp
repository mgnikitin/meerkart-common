#include <gtest/gtest.h>
#include "meerkart-common/rtree.h"

using namespace geomap;

TEST(RTree, insert)
{
    RTree<size_t, double, 8> rtree;

    rtree.insert(RTreeRect<double>{0, 0, 2, 2}, 1);
    rtree.insert(RTreeRect<double>{5, 5, 7, 7}, 2);
    rtree.insert(RTreeRect<double>{8, 5, 9, 6}, 3);
    rtree.insert(RTreeRect<double>{7, 1, 9, 2}, 4);

    std::vector<size_t> act;
    auto ret = rtree.search(RTreeRect<double>{6, 4, 10, 6}, [&act](size_t idx) {
        act.push_back(idx);
        return true;
    });

    std::vector<size_t> exp = { 2, 3};
    EXPECT_EQ(ret, 2);
    EXPECT_EQ(act, exp);
}

//TEST(RTree, remove)
//{
//    RTree<size_t, double, 8> rtree;

//    rtree.insert(RTreeRect<double>{0, 0, 2, 2}, 1);
//    rtree.insert(RTreeRect<double>{5, 5, 7, 7}, 2);

//    rtree.remove(RTreeRect<double>{6, 4, 10, 6}, 2);

//    EXPECT_TRUE(false);
//}
