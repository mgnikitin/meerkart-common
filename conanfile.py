from conans import ConanFile, CMake


class MeerkartCommonConan(ConanFile):
    name = "meerkart-common"
    version = "0.1"
    license = "<Put the package license here>"
    author = "mgnikitin@gmail.com"
    url = "https://mgnikitin@bitbucket.org/mgnikitin/meerkart-common"
    description = "Common files for meerkart project"
    settings = "os", "compiler", "build_type", "arch"
    generators = "cmake"
    exports_sources = "src/*", "test/*", "CMakeLists.txt"

    options = {
        "shared": [True, False],
        "fPIC": [True, False],
        "with_tests": [True, False],
    }

    default_options = {
        "shared": False,
        "fPIC": True,
        "with_tests": True,
    }

    def requirements(self):
        self.requires("boost/1.75.0")
        self.requires("proj/6.3.1")
        self.requires("protobuf/3.11.4")
        if self.options.get_safe("with_tests"):
            self.requires("gtest/1.8.1")

    def config_options(self):
        if self.settings.os == "Windows":
            del self.options.fPIC

    def configure(self):
        if self.options.shared:
            del self.options.fPIC

    def build(self):
        cmake = CMake(self)
        if self.options.get_safe("with_tests"):
            cmake.definitions["WITH_TESTS"] = "ON"
        cmake.configure()
        cmake.build()
        if self.options.get_safe("with_tests"):
            cmake.test(output_on_failure=True)

    def package(self):
        self.copy("*.h", dst="include", src="src/include")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.dylib*", dst="lib", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["meerkart-common"]
