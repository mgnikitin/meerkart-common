#include "tiledata_serializer.h"
#include "tiledata.pb.h"

namespace geomap {

void convert(const Vertex2m& from, pb_geomap::Vertex2m& to) {

    to.set_x(from.x.value());
    to.set_y(from.y.value());
}

void convert(const Vertex2f& from, pb_geomap::Vertex2f& to) {

    to.set_x(from.x);
    to.set_y(from.y);
}

Vertex2m convert(const pb_geomap::Vertex2m& from) {

    return {units::meter(from.x()), units::meter(from.y())};
}

Vertex2f convert(const pb_geomap::Vertex2f& from) {

    return {from.x(), from.y()};
}

void convert(const Rect2m& from, pb_geomap::Rect2m& to) {

    pb_geomap::Vertex2m* tr = to.mutable_tr();
    convert(from.tr, *tr);
    pb_geomap::Vertex2m* bl = to.mutable_bl();
    convert(from.bl, *bl);
}

Rect2m convert(const pb_geomap::Rect2m& from) {

    return Rect2m{convert(from.bl()), convert(from.tr())};
}

void convert(const TileData::Property& from, pb_geomap::Property& to) {

    to.set_name(from.name);
    to.set_value(from.value);
}

TileData::Property convert(const pb_geomap::Property& from) {

    return {from.name(), from.value()};
}

void convert(const TileData::Point& from, pb_geomap::Point& to) {

    auto* coord = to.mutable_coord();
    convert(from.coord, *coord);
    // properties
    for(auto& p : from.properties) {
        auto* prop = to.add_properties();
        convert(p, *prop);
    }
}

TileData::Point convert(const pb_geomap::Point& from) {

    std::vector<TileData::Property> properties;
    for(auto i = 0; i < from.properties_size(); ++i) {
        auto& p = from.properties(i);
        properties.push_back(convert(p));
    }
    return TileData::Point{convert(from.coord()), std::move(properties)};
}

void convert(const TileData::CoordVec& from, pb_geomap::CoordArray& to) {

    for(auto& p : from) {
        to.add_data(p.x);
        to.add_data(p.y);
    }
}

TileData::CoordVec convert(const pb_geomap::CoordArray& from) {

    TileData::CoordVec ret;
    for(auto i = 0; i < from.data_size()/2; ++i) {
        auto x = from.data(i*2);
        auto y = from.data(i*2+1);
        ret.push_back(Vertex2f{x, y});
    }

    return ret;
}

void convert(const TileData::Line& from, pb_geomap::Line& to) {

    for(auto& p : from.coords) {
        auto* coord = to.add_coords();
        convert(p, *coord);
    }
    // properties
    for(auto& p : from.properties) {
        auto* prop = to.add_properties();
        convert(p, *prop);
    }
}

TileData::Line convert(const pb_geomap::Line& from) {

    std::deque<Vertex2f> coords;
    for(auto i = 0; i < from.coords_size(); ++i) {
        auto& l = from.coords(i);
        coords.push_back(convert(l));
    }

    std::vector<TileData::Property> properties;
    for(auto i = 0; i < from.properties_size(); ++i) {
        auto& p = from.properties(i);
        properties.push_back(convert(p));
    }

    return TileData::Line{std::move(coords), std::move(properties)};
}

void convert(const TileData::Area& from, pb_geomap::Area& to) {

    auto* outer_p = to.mutable_outer();
    convert(from.outer, *outer_p);

    for(auto& inner : from.inners) {
        auto* inner_p = to.add_inners();
        convert(inner, *inner_p);
    }
    // properties
    for(auto& p : from.properties) {
        auto* prop = to.add_properties();
        convert(p, *prop);
    }
}

TileData::Area convert(const pb_geomap::Area& from) {

    auto outer = convert(from.outer());

    std::deque<TileData::CoordVec> inners;
    for(auto i = 0; i < from.inners_size(); ++i) {
        auto& inner = from.inners(i);
        inners.push_back(convert(inner));
    }

    std::vector<TileData::Property> properties;
    for(auto i = 0; i < from.properties_size(); ++i) {
        auto& p = from.properties(i);
        properties.push_back(convert(p));
    }

    return TileData::Area{std::move(outer), std::move(inners), std::move(properties)};
}

TileInfo convert(const pb_geomap::TileInfo& from) {

    return TileInfo(TileId{from.id()},
                    from.revision(),
                    from.provider_id(),
                    from.data_key(),
                    from.zoom_level(),
                    convert(from.border_rect()));
}

std::vector<unsigned char> TileDataSerializer::serialize(const TileData& td) {

    pb_geomap::TileData tile_data;

    // info
    pb_geomap::TileInfo* tile_info = tile_data.mutable_info();
    tile_info->set_id(td.m_info.m_id.idx);
    tile_info->set_revision(td.m_info.m_revision);
    tile_info->set_provider_id(td.m_info.m_provider_id);
    tile_info->set_data_key(td.m_info.m_data_key);
    tile_info->set_zoom_level(td.m_info.m_zoom_level);
    pb_geomap::Rect2m* border_rect = tile_info->mutable_border_rect();
    convert(td.m_info.m_border_rect, *border_rect);

    // origin
    auto origin = tile_data.mutable_origin();
    convert(td.m_origin, *origin);

    // points
    for(auto& p : td.m_points) {
        pb_geomap::Point* point = tile_data.add_points();
        convert(p, *point);
    }

    // lines
    for(auto& l : td.m_lines) {
        auto* line = tile_data.add_lines();
        convert(l, *line);
    }

    // areas
    for(auto& a : td.m_areas) {
        auto* area = tile_data.add_areas();
        convert(a, *area);
    }

    std::string binary;
    tile_data.SerializeToString(&binary);

    return std::vector<unsigned char>(binary.begin(), binary.end());
}

TileData TileDataSerializer::deserialize(const std::vector<unsigned char>& data) {

    pb_geomap::TileData tile_data;
    tile_data.ParseFromArray(data.data(), data.size());

    if(!tile_data.has_info()) {
        throw std::runtime_error("TileDataSerializer: unable to get tile info");
    }
    TileInfo tile_info = convert(tile_data.info());

    TileData ret(std::move(tile_info), convert(tile_data.origin()));

    for(auto i = 0; i < tile_data.points_size(); ++i) {
        auto& p = tile_data.points(i);
        ret.m_points.push_back(convert(p));
    }

    for(auto i = 0; i < tile_data.lines_size(); ++i) {
        auto& l = tile_data.lines(i);
        ret.m_lines.push_back(convert(l));
    }

    for(auto i = 0; i < tile_data.areas_size(); ++i) {
        auto& a = tile_data.areas(i);
        ret.m_areas.push_back(convert(a));
    }

    return ret;
}

} // namespace geomap
