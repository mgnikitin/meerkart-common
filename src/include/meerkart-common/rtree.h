#ifndef RTREE_H
#define RTREE_H

#include <algorithm>
#include <functional>
#include <cassert>
#include <memory>
#include <vector>
#include <meerkart-common/vector_types.h>

#include <boost/optional.hpp>

namespace geomap {

template <typename T>
using RTreeVertex = Vertex2<T>;

template <typename T>
struct RTreeRect : Rect<T> {

    RTreeRect(T xl, T yb, T xr, T yt)
        : Rect<T>{Vertex2<T>{xl, yb}, Vertex2<T>{xr, yt}}
    {}

    explicit RTreeRect(Rect<T> rect)
        : Rect<T>(rect)
    {}

    double volume() const noexcept { return (Rect<T>::tr.x - Rect<T>::bl.x) * (Rect<T>::tr.y - Rect<T>::bl.y); }

    RTreeRect<T> combined(const RTreeRect<T> &rect) const {

        return RTreeRect<T> {std::min(Rect<T>::bl.x, rect.bl.x),
                    std::min(Rect<T>::bl.y, rect.bl.y),
                    std::max(Rect<T>::tr.x, rect.tr.x),
                    std::max(Rect<T>::tr.y, rect.tr.y)};
    }
};

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
class RTreeNode;

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
struct RTreeBranch {
    typedef RTreeRect<ELEMENTTYPE> Rect;
    typedef RTreeNode<DATATYPE, ELEMENTTYPE, MAXNODES> Node;

    RTreeBranch()
        : m_rect(ELEMENTTYPE(0), ELEMENTTYPE(0),ELEMENTTYPE(0), ELEMENTTYPE(0))
    {}

    RTreeBranch(Rect rect, boost::optional<DATATYPE> data, std::unique_ptr<Node> node)
        : m_rect(rect), m_data(data), m_node(std::move(node))
    {}

    RTreeBranch(Rect rect, std::unique_ptr<Node> node)
        : m_rect(rect), m_data(boost::none), m_node(std::move(node))
    {}

    Rect m_rect;
    boost::optional<DATATYPE> m_data;
    std::unique_ptr<Node> m_node;


};

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
class RTreeGroup {
public:

    typedef RTreeRect<ELEMENTTYPE> Rect;
//    typedef RTreeBranch<DATATYPE, ELEMENTTYPE, MAXNODES> Branch;

    RTreeGroup()
        : m_cover(ELEMENTTYPE(0),ELEMENTTYPE(0),ELEMENTTYPE(0),ELEMENTTYPE(0))
        , m_count(0)
    {}

    void classify(Rect rect, size_t index);

    size_t m_indexes[MAXNODES];
    Rect m_cover;
    size_t m_count;
};

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
void RTreeGroup<DATATYPE, ELEMENTTYPE, MAXNODES>::classify(Rect rect, size_t index)
{
    m_indexes[m_count] = index;

  if (m_count == 0)
  {
    m_cover = rect;
  }
  else
  {
    m_cover = m_cover.combined(rect);
  }
  ++m_count;
}

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
class RTreeNode {
public:

    typedef RTreeNode<DATATYPE, ELEMENTTYPE, MAXNODES> Node;
    typedef RTreeRect<ELEMENTTYPE> Rect;
    typedef RTreeBranch<DATATYPE, ELEMENTTYPE, MAXNODES> Branch;
    typedef RTreeGroup<DATATYPE, ELEMENTTYPE, MAXNODES> Group;
    static constexpr size_t MINNODES = MAXNODES/2;

    Branch m_branches[MAXNODES];
    size_t m_count;
    size_t m_level;

    RTreeNode()
        : m_count(0)
        , m_level(0)
    {}

    bool is_internal_node() const noexcept { return (m_level > 0); } // Not a leaf, but a internal node
    bool is_leaf() const noexcept { return (m_level == 0); } // A leaf, contains data

    size_t optimalBranch(const Rect &rect) const;

    void add_branch(Branch&& branch);
    void remove_branch(size_t index);

    std::pair<size_t, size_t> pick_seed(double areas[], Branch branches[]);
    void partition(Branch&& branch, Node &node);
    Rect node_cover() const;
};

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
size_t RTreeNode<DATATYPE, ELEMENTTYPE, MAXNODES>::optimalBranch(const Rect &rect) const {

    assert(m_count > 0);

    auto irect0 = m_branches[0].m_rect;
    auto resarea = irect0.volume();
    auto crect0 = rect.combined(irect0);
    auto resincr = crect0.volume() - resarea;
    size_t resindex = 0;

    for(size_t i = 1; i < m_count; ++i) {
        const auto irect = m_branches[i].m_rect;
        auto area = irect.volume();
        auto crect = rect.combined(irect);
        auto increase = crect.volume() - area;

        if(resincr > increase) {
            resincr = increase;
            resarea = area;
            resindex = i;
        }
        else if((resincr == increase) && (area < resarea)) {
            resincr = increase;
            resarea = area;
            resindex = i;
        }
    }

    return resindex;
}

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
void RTreeNode<DATATYPE, ELEMENTTYPE, MAXNODES>::add_branch(Branch &&branch) {

    assert(m_count < MAXNODES);

    m_branches[m_count++] = std::move(branch);
}

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
void RTreeNode<DATATYPE, ELEMENTTYPE, MAXNODES>::remove_branch(size_t index) {

    assert(m_count > 0);

    if(1 == m_count) {
        m_branches[index].m_node.reset(nullptr);
    }
    else {
        m_branches[index] = std::move(m_branches[m_count - 1]);
    }
    --m_count;
}

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
std::pair<size_t, size_t> RTreeNode<DATATYPE, ELEMENTTYPE, MAXNODES>::pick_seed(double areas[MAXNODES + 1], Branch branches[MAXNODES + 1]) {

    size_t seed0 = 0;
    size_t seed1 = 1;
    auto worst = branches[0].m_rect.combined(branches[1].m_rect).volume() - areas[0] - areas[1];

    for(size_t i = 0; i < MAXNODES; ++i) {
        for(size_t j = i+1; j < MAXNODES + 1; ++j) {
            auto r = branches[i].m_rect.combined(branches[j].m_rect);
            auto incr = r.volume() - areas[i] - areas[j];
            if(incr > worst) {
                worst = incr;
                seed0 = i;
                seed1 = j;
            }
        }
    }

    return {seed0, seed1};
}

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
void RTreeNode<DATATYPE, ELEMENTTYPE, MAXNODES>::partition(Branch&& branch, Node& node) {

    assert(m_count == MAXNODES);
    double areas[MAXNODES + 1];
    Group group0;
    Group group1;
    constexpr size_t total = MAXNODES + 1;

    // init
    size_t last = MAXNODES;
    Branch branches[MAXNODES + 1];

    for(size_t i = 0; i < MAXNODES; ++i) {
        areas[i] = m_branches[i].m_rect.volume();
        branches[i] = std::move(m_branches[i]);

    }
    areas[MAXNODES] = branch.m_rect.volume();
    branches[MAXNODES] = std::move(branch);

    std::pair<size_t, size_t> seed_p = pick_seed(areas, branches);

    std::swap(branches[seed_p.first], branches[last]);
    std::swap(areas[seed_p.first], areas[last]);
    group0.classify(branches[last].m_rect, last);
    last--;
    std::swap(branches[seed_p.second], branches[last]);
    std::swap(areas[seed_p.second], areas[last]);
    group1.classify(branches[last].m_rect, last);
    last--;

    auto get_group = [&]() -> std::pair<size_t, Group&> {

        size_t index_ret = 0;
        boost::optional<Group&> group_ret = group0;
        auto biggestDiff = (double) -1;
        for(size_t index=0; index <= last; ++index)
        {
            Rect cur_rect = branches[index].m_rect;
            Rect rect0 = cur_rect.combined(group0.m_cover);
            Rect rect1 = cur_rect.combined(group1.m_cover);
            auto growth0 = rect0.volume() - group0.m_cover.volume();
            auto growth1 = rect1.volume() - group1.m_cover.volume();
            auto diff = growth1 - growth0;
            Group& group = (diff >= 0) ? group0 : group1;
            if(diff < 0)
            {
              diff = -diff;
            }

            if(diff > biggestDiff)
            {
                biggestDiff = diff;
                index_ret = index;
                group_ret.emplace(group);
            }
            else if((diff == biggestDiff) && (group.m_count < group_ret->m_count))
            {
                index_ret = index;
                group_ret.emplace(group);
            }
        }

        return {index_ret, *group_ret};
    };

    while ((group0.m_count < (total - MINNODES))
         && (group1.m_count < (total - MINNODES)))
    {
        std::pair<size_t, Group&> p = get_group();
        std::swap(branches[p.first], branches[last]);
        std::swap(areas[p.first], areas[last]);
        p.second.classify(branches[last].m_rect, last);
        if(0 == last) {
            break;
        }
        last--;
    }

    // If one group too full, put remaining rects in the other
    if((group0.m_count + group1.m_count) < total)
    {
        assert(last < total);
        Group& group = (group0.m_count >= (total - MINNODES)) ? group1 : group0;
        for(size_t index=0; index <= last; ++index)
        {
            group.classify(branches[index].m_rect, index);
        }
    }

    assert((group0.m_count + group1.m_count) == total);
    assert((group0.m_count >= MINNODES) &&
          (group1.m_count >= MINNODES));

    auto fill_node = [&branches](Group& group, Node& node) {

        node.m_count = 0;
        for(size_t idx = 0; idx < group.m_count; ++idx) {
            node.add_branch(std::move(branches[group.m_indexes[idx]]));
        }
    };

    fill_node(group1, node);
    fill_node(group0, *this);
}

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
RTreeRect<ELEMENTTYPE> RTreeNode<DATATYPE, ELEMENTTYPE, MAXNODES>::node_cover() const {

    if(m_count == 0) {
        assert(false);
        return Rect(ELEMENTTYPE(0), ELEMENTTYPE(0), ELEMENTTYPE(0), ELEMENTTYPE(0));
    }
    Rect rect = m_branches[0].m_rect;
    for(size_t idx = 1; idx < m_count; ++idx) {
        rect = rect.combined(m_branches[idx].m_rect);
    }

    return rect;
}

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
class RTree {
public:

    typedef RTreeRect<ELEMENTTYPE> Rect;
    typedef RTreeBranch<DATATYPE, ELEMENTTYPE, MAXNODES> Branch;
    typedef RTreeNode<DATATYPE, ELEMENTTYPE, MAXNODES> Node;
    typedef std::function<bool(DATATYPE)> callback_t;

    RTree();

    void insert(const Rect &rect, DATATYPE data);

    void remove(const Rect &rect, DATATYPE data);

    int search(const Rect &rect, callback_t) const;

private:

    std::unique_ptr<Node> m_root;

    void insert(const Rect &rect, boost::optional<DATATYPE> data, size_t level);
    std::unique_ptr<Node> insert_rec(Branch&& branch, Node &parent, size_t level);

    bool remove_rec(const Branch& branch, Node &parent, std::vector<std::unique_ptr<Node> > &reinsert_vec);

    bool search(Node& a_node, const Rect& a_rect, int& a_foundCount, callback_t callback) const;

    std::unique_ptr<RTreeNode<DATATYPE, ELEMENTTYPE, MAXNODES>> split(Branch &&branch, Node &parent);

};

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
RTree<DATATYPE, ELEMENTTYPE, MAXNODES>::RTree()
    : m_root(std::make_unique<Node>())
{
}

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
void RTree<DATATYPE, ELEMENTTYPE, MAXNODES>::insert(const Rect &rect, DATATYPE data) {

    insert(rect, data, 0);
}

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
void RTree<DATATYPE, ELEMENTTYPE, MAXNODES>::remove(const Rect &rect, DATATYPE data) {

    assert(m_root);

    std::vector<std::unique_ptr<Node>> reinsert_vec;
    if(!remove_rec(Branch{rect, data, nullptr}, *m_root, reinsert_vec)) {
        for(std::unique_ptr<Node>& node : reinsert_vec) {

            for(auto index = 0; index < node->m_count; ++index) {
                auto& branch = node->m_branches[index];
                insert(branch.m_rect, branch.m_data, node->m_level);
            }
        }

        // Check for redundant root (not leaf, 1 child) and eliminate
        if(m_root->m_count == 1 && m_root->is_internal_node()) {

            auto tmp = std::move(m_root->m_branches[0].m_node);
            m_root = std::move(tmp);
        }
    }
}

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
int RTree<DATATYPE, ELEMENTTYPE, MAXNODES>::search(const Rect &rect, callback_t callback) const
{
  int foundCount = 0;
  assert(m_root);

  search(*m_root, rect, foundCount, callback);

  return foundCount;
}

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
void RTree<DATATYPE, ELEMENTTYPE, MAXNODES>::insert(const Rect &rect, boost::optional<DATATYPE> data, size_t level) {

    assert(m_root);

    std::unique_ptr<Node> node = insert_rec(Branch{rect, data, nullptr}, *m_root, level);
    if(node) {
        auto new_root = std::make_unique<Node>();
        new_root->m_level = m_root->m_level + 1;
        auto root_cover = m_root->node_cover();
        auto node_cover = node->node_cover();
        new_root->add_branch(Branch{root_cover, std::move(m_root)});
        new_root->add_branch(Branch{node_cover, std::move(node)});
        m_root = std::move(new_root);
    }
}

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
std::unique_ptr<typename RTree<DATATYPE, ELEMENTTYPE, MAXNODES>::Node> RTree<DATATYPE, ELEMENTTYPE, MAXNODES>::insert_rec(Branch&& branch, Node &parent, size_t level) {

    if(parent.m_level > level) { // go futher
        size_t idx = parent.optimalBranch(branch.m_rect);
        if(auto new_node = insert_rec(std::move(branch), *parent.m_branches[idx].m_node, level)) {
            // Child was split
            parent.m_branches[idx].m_rect = parent.m_branches[idx].m_node->node_cover();
            auto node_cover = new_node->node_cover();
            Branch new_branch(node_cover, std::move(new_node));
            if(parent.m_count < MAXNODES) {
                parent.add_branch(std::move(new_branch));
                return nullptr;
            }
            return split(std::move(new_branch), parent);
        }
        else {
            parent.m_branches[idx].m_rect = parent.m_branches[idx].m_rect.combined(branch.m_rect);
            return nullptr;
        }
    }
    else { // leaf
        if(parent.m_count < MAXNODES) {
            parent.add_branch(std::move(branch));
            return nullptr;
        }

        return split(std::move(branch), parent);
    }
}

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
bool RTree<DATATYPE, ELEMENTTYPE, MAXNODES>::remove_rec(const Branch& branch, Node &parent, std::vector<std::unique_ptr<Node>>& reinsert_vec) {

    if(parent.is_internal_node()) { // go futher
        for(size_t index=0; index < parent.m_count; ++index) {

            if(branch.m_rect.touches(parent.m_branches[index].m_rect)) {
                if(!remove_rec(branch, *parent.m_branches[index].m_node, reinsert_vec)) {
                    if(parent.m_branches[index].m_node->m_count >= RTreeNode<DATATYPE, ELEMENTTYPE, MAXNODES>::MINNODES) {
                        parent.m_branches[index].m_rect = parent.m_branches[index].m_node->node_cover();
                    }
                    else {
                        reinsert_vec.push_back(std::move(parent.m_branches[index].m_node));
                        parent.remove_branch(index);
                    }
                    return false;
                }
            }
        }
    }
    else { // leaf
        assert(parent.is_leaf());
        for(size_t index=0; index < parent.m_count; ++index) {

            if(parent.m_branches[index].m_data == branch.m_data) {
                parent.remove_branch(index);
                return false;
            }
        }
    }

    return true;
}

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
bool RTree<DATATYPE, ELEMENTTYPE, MAXNODES>::search(Node& a_node, const Rect& a_rect, int& a_foundCount, callback_t callback) const
{
    assert(a_node.m_level >= 0);

    if(a_node.is_internal_node())
    {
        for(size_t index=0; index < a_node.m_count; ++index)
        {
          if(a_rect.touches(a_node.m_branches[index].m_rect))
          {
              assert(a_node.m_branches[index].m_node);
            if(!search(*a_node.m_branches[index].m_node, a_rect, a_foundCount, callback))
            {
              return false; // Don't continue searching
            }
          }
        }
    }
    else // leaf
    {
        assert(a_node.is_leaf());
        for(size_t index=0; index < a_node.m_count; ++index)
        {
            if(a_rect.touches(a_node.m_branches[index].m_rect))
            {
                auto& data_opt = a_node.m_branches[index].m_data;
                if(!data_opt.is_initialized()) {
                    assert(false);
                    continue;
                }
                DATATYPE& id = *data_opt;
                ++a_foundCount;
                if(!callback(id))
                {
                    return false; // Don't continue searching
                }
            }
        }
    }

  return true; // Continue searching
}

template <typename DATATYPE, typename ELEMENTTYPE, size_t MAXNODES>
std::unique_ptr<RTreeNode<DATATYPE, ELEMENTTYPE, MAXNODES>> RTree<DATATYPE, ELEMENTTYPE, MAXNODES>::split(Branch&& branch, Node &parent) {

    auto node = std::make_unique<Node>(); // allocate();
    node->m_level = parent.m_level;

    parent.partition(std::move(branch), *node);

    return node;
}

} // namespace geomap {

#endif // RTREE_H
