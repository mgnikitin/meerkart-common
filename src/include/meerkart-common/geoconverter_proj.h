#ifndef GEOCONVERTER_PROJ_H
#define GEOCONVERTER_PROJ_H

#include <proj.h>
#include <memory>
#include <meerkart-common/vector_types.h>

namespace geomap {

//---------------------------------------------------------------------------------------
class ProjContextDeleter {
public:

    void operator()(PJ_CONTEXT* c) {
        proj_context_destroy(c);
    }
};

//---------------------------------------------------------------------------------------
class ProjDeleter {
public:

    void operator()(PJ* p) {
        proj_destroy(p);
    }
};

//---------------------------------------------------------------------------------------
class GeoConverterProj {
public:
    GeoConverterProj(const char *path = "./res");
    ~GeoConverterProj() = default;

    Vertex2m geoToPlain(const Vertex2d geo_crd);
    Vertex2d plainToGeo(const Vertex2m p);

    units::meter CorrectPlainX(units::meter X);

private:

    std::unique_ptr<PJ_CONTEXT, ProjContextDeleter> m_proj_ctx;
    std::unique_ptr<PJ, ProjDeleter> m_transf;
};

} // namespace geomap {

#endif // GEOCONVERTER_PROJ_H
