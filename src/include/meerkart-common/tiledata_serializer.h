#ifndef TILEDATA_SERIALIZER_H
#define TILEDATA_SERIALIZER_H

#include "tiledata.h"

namespace geomap {

class TileDataSerializer {
public:

    std::vector<unsigned char> serialize(const TileData& td);
    TileData deserialize(const std::vector<unsigned char>& data);

};

}

#endif // TILEDATA_SERIALIZER_H
