#ifndef CONCURRENTQUEUE_H
#define CONCURRENTQUEUE_H

#include <queue>
#include <mutex>
#include <memory>

namespace geomap {

template<typename T>
class ConcurrentQueue {
public:
    ConcurrentQueue() = default;
    ConcurrentQueue(const ConcurrentQueue&);
    ConcurrentQueue& operator=(const ConcurrentQueue&) = delete;

    void push(T d);

    bool try_pop(T& d);
    std::shared_ptr<T> try_pop();

    bool empty() const;

private:

    mutable std::mutex m_mutex;
    std::queue<T> m_queue;
};

template<typename T>
ConcurrentQueue<T>::ConcurrentQueue(const ConcurrentQueue<T>& other) {

    std::lock_guard<std::mutex> lock(other.m_mutex);
    m_queue = other.m_queue;
}

template<typename T>
void ConcurrentQueue<T>::push(T d) {

    std::lock_guard<std::mutex> lock(m_mutex);
    m_queue.push(d);
}

template<typename T>
bool ConcurrentQueue<T>::try_pop(T& d) {

    std::lock_guard<std::mutex> lock(m_mutex);
    if(m_queue.empty()) {
        return false;
    }
    d = std::move(m_queue.front());
    m_queue.pop();
    return true;
}

template<typename T>
std::shared_ptr<T> ConcurrentQueue<T>::try_pop() {

    std::lock_guard<std::mutex> lock(m_mutex);
    if(m_queue.empty()) {
        return std::shared_ptr<T>();
    }

    std::shared_ptr<T> ret = std::make_shared<T>(m_queue.front());
    m_queue.pop();
    return ret;
}

template<typename T>
bool ConcurrentQueue<T>::empty() const {

    std::lock_guard<std::mutex> lock(m_mutex);
    return m_queue.empty();
}

} // namespace geomap

#endif // CONCURRENTQUEUE_H
