#ifndef TILEDATA_H
#define TILEDATA_H

#include <meerkart-common/vector_types.h>
#include <deque>
#include <vector>
#include <functional>
#include <string>

namespace geomap {

struct TileId {

    uint64_t idx;
};

inline bool operator==(TileId a, TileId b) {
    return a.idx == b.idx;
}

struct TileInfo {

    TileInfo(TileId id,
             uint32_t revision,
             const std::string& provider_id,
             const std::string& data_key,
             uint32_t zoom_level,
             Rect2m border_rect)
        : m_id(id)
        , m_revision(revision)
        , m_provider_id(provider_id)
        , m_data_key(data_key)
        , m_zoom_level(zoom_level)
        , m_border_rect(border_rect)
    {}

    TileId m_id;
    uint32_t m_revision;
    std::string m_provider_id;
    std::string m_data_key;
    uint32_t m_zoom_level;
    Rect2m m_border_rect;
};

struct TileData {

    struct Property {
        std::string name;
        std::string value;
    };

    struct Point {
        Vertex2f coord;
        std::vector<Property> properties;
    };

    using CoordVec = std::deque<Vertex2f>;

    struct Line {
        std::deque<Vertex2f> coords;
        std::vector<Property> properties;
    };

    struct Area {
        CoordVec outer;
        std::deque<CoordVec> inners;
        std::vector<TileData::Property> properties;
    };

    explicit TileData(TileInfo&& info, Vertex2m origin)
        : m_info(std::move(info))
        , m_origin(origin)
    {}

    TileInfo m_info;
    // basis for coords
    Vertex2m m_origin;
    // points, related coords
    std::deque<Point> m_points;
    // lines, related coords
    std::deque<Line> m_lines;
    // areas, related coords
    std::deque<Area> m_areas;
};

} // namespace geomap {

namespace std
{
    template<> struct hash<geomap::TileId>
    {
        std::size_t operator()(const geomap::TileId& id) const noexcept {
            return std::hash<size_t>{}(id.idx);
        }
    };
}

#endif // TILEDATA_H
