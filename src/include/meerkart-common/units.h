#ifndef UNITS_H
#define UNITS_H

#include <ratio>

namespace geomap::units {

template<class T>
class distance {
private:

    double m_value;

public:

    using value_type = decltype(m_value);

    constexpr explicit distance(double value)
        : m_value(value)
    {}

    distance(const distance<T>&) = default;

    constexpr double value() const  noexcept { return m_value; }

    constexpr distance<T> operator-() const {
        return distance<T>{-m_value};
    }

    constexpr bool operator <(double b) const {
        return m_value < b;
    }
    constexpr bool operator <(distance<T> b) const {
        return m_value < b.m_value;
    }
    constexpr bool operator >(double b) const {
        return m_value > b;
    }
    constexpr bool operator >(distance<T> b) const {
        return m_value > b.m_value;
    }
    constexpr bool operator <=(double b) const {
        return m_value <= b;
    }
    constexpr bool operator <=(distance<T> b) const {
        return m_value <= b.m_value;
    }
    constexpr bool operator >=(double b) const {
        return m_value >= b;
    }
    constexpr bool operator >=(distance<T> b) const {
        return m_value >= b.m_value;
    }

    template<class U>
    constexpr friend distance<U> operator+(distance<U> a, distance<U> b) noexcept;
    template<class U>
    constexpr friend distance<U> operator-(distance<U> a, distance<U> b) noexcept;

    template<class U>
    constexpr friend distance<U> operator*(distance<U> a, double b) noexcept;
    template<class U>
    constexpr friend double operator*(distance<U> a, distance<U> b) noexcept;

    template<class U>
    constexpr friend distance<U> operator/(distance<U> a, double b);
    template<class U>
    constexpr friend double operator/(distance<U> a, distance<U> b);
    template<class U>
    constexpr friend bool operator==(distance<U> a, distance<U> b);
};

template<class T>
constexpr distance<T> operator+(distance<T> a, distance<T> b) noexcept {
    return distance<T>{a.value()+b.value()};
}

template<class T>
constexpr distance<T> operator-(distance<T> a, distance<T> b) noexcept {
    return distance<T>{a.value()-b.value()};
}

template<class T>
constexpr distance<T> operator*(distance<T> a, double b) noexcept {
    return distance<T>{a.value() * b};
}

template<class T>
constexpr double operator*(distance<T> a, distance<T> b) noexcept {
    return a.value() * b.value();
}

template<class T>
constexpr distance<T> operator/(distance<T> a, double b) {
    return distance<T>{a.value() / b};
}

template<class T>
constexpr double operator/(distance<T> a, distance<T> b) {
    return a.value() / b.value();
}

template<class T>
constexpr bool operator==(distance<T> a, distance<T> b) {
    return a.value() == b.value();
}

using meter = distance<std::ratio<1> >;
using milimeter = distance<std::ratio<1, 1000> >;

} // namespace geomap::units

#endif // UNITS_H
