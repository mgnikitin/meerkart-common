#ifndef VECTOR_TYPES_H
#define VECTOR_TYPES_H

#include <cmath>
#include <variant>
#include <meerkart-common/units.h>

namespace geomap {

//------------------------------------------------------------------------------------------------------------------
template <typename T> struct Vertex2 {
    T x, y;
};

template <typename T>
inline bool operator ==(Vertex2<T> a, Vertex2<T> b) {
    return a.x == b.x && a.y == b.y;
}

template <typename T>
inline Vertex2<T> operator +(Vertex2<T> a, Vertex2<T> b) {
    return Vertex2<T>{a.x + b.x, a.y + b.y };
}

template <typename T>
inline Vertex2<T> operator -(Vertex2<T> a, Vertex2<T> b) {
    return Vertex2<T>{a.x - b.x, a.y - b.y };
}

//------------------------------------------------------------------------------------------------------------------
typedef Vertex2<float> Vertex2f;
using Vertex2d = Vertex2<double>;
using Vertex2m = Vertex2<units::meter>;

template<typename T>
struct Rect {
    Vertex2<T> bl;
    Vertex2<T> tr;

    T width() const noexcept { return tr.x - bl.x; }
    T height() const noexcept { return tr.y - bl.y; }
    bool contains(Vertex2<T> v) const noexcept {
        return !(v.x < bl.x || v.x > tr.x || v.y < bl.y || v.y > tr.y);
    }

    /// \brief Проверка на пересечение или попадание прямоугольных областей
    bool intersects(const Rect<T> r) const noexcept {return !(r.bl.x >= tr.x || r.tr.x <= bl.x || r.bl.y >= tr.y || r.tr.y <= bl.y);}
    /// \brief Проверка на касание, пересечение или попадание прямоугольных областей. Возвращает true если участки касаются, пересекаются или один из участков содержит другой.
    bool touches(const Rect<T> r) const noexcept {return !(r.bl.x > tr.x || r.tr.x < bl.x || r.bl.y > tr.y || r.tr.y < bl.y);}

    Vertex2<T> nears(Vertex2<T> v) const {

        if(v.x < bl.x) { v.x = bl.x; }
        else if(v.x > tr.x) { v.x = tr.x; }
        if(v.y < bl.y) { v.y = bl.y; }
        else if(v.y > tr.y) { v.y = tr.y; }

        return v;
    }
};

using Rect2m = Rect<units::meter>;

} // namespace geomap

#endif // VECTOR_TYPES_H
