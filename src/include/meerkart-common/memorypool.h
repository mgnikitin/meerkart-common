#ifndef MEMORYPOOL_H
#define MEMORYPOOL_H

#include <mutex>
#include <deque>
#include <cassert>

namespace geomap {

template<typename T>
class MemoryPool {
public:

    struct Element {
        T d;
        bool filled;
    };

    MemoryPool() = default;
    MemoryPool& operator=(const MemoryPool& ) = delete;

    T& data(size_t idx) { return m_data[idx].d; }
    const T& data(size_t idx) const { return m_data[idx].d; }

    size_t put(T &&d);
    void release(size_t idx);

private:

    std::deque<Element> m_data;
    std::deque<size_t> m_available;
};

template <typename T>
size_t MemoryPool<T>::put(T&& d) {

    if(!m_available.empty()) {
        size_t idx = m_available.back();
        assert(!m_data[idx].filled);
        m_data[idx].d = std::forward<T>(d);
        m_data[idx].filled = true;
        m_available.pop_back();
        return idx;
    }

    m_data.push_back({std::forward<T>(d), true});
    return m_data.size()-1;
}

template <typename T>
void MemoryPool<T>::release(size_t idx) {

    assert(m_data.size() > idx);
    assert(m_data[idx].filled);

    m_data[idx].filled = false;
    m_available.emplace_back(idx);
}

template<typename T>
class MemoryPoolThreaded {
public:

    using Element = typename MemoryPool<T>::Element;

    MemoryPoolThreaded() = default;
    MemoryPoolThreaded& operator=(const MemoryPoolThreaded& ) = delete;

    T& data(size_t idx) { return m_pool.data(idx); }
    const T& data(size_t idx) const { return m_pool.data(idx); }

    size_t put(T &&d);
    void release(size_t idx);

private:

    MemoryPool<T> m_pool;
    std::mutex m_mutex;
};

template <typename T>
size_t MemoryPoolThreaded<T>::put(T&& d) {

    std::lock_guard<std::mutex> lock(m_mutex);

    return m_pool.put(std::move(d));
}

template <typename T>
void MemoryPoolThreaded<T>::release(size_t idx) {

    std::lock_guard<std::mutex> lock(m_mutex);

    m_pool.release(idx);
}

} // namespace geomap

#endif // MEMORYPOOL_H
