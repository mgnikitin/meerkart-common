#include "geoconverter_proj.h"

namespace geomap {

constexpr units::meter R{6378137.};
constexpr units::meter PLAIN_X_LEN = R*M_PI*2.;
constexpr units::meter PLAIN_X_MAX = PLAIN_X_LEN/2.;
constexpr units::meter PLAIN_X_MIN = -PLAIN_X_LEN/2.;

GeoConverterProj::GeoConverterProj(const char* path)
    : m_proj_ctx(proj_context_create())
{
    proj_context_set_search_paths(m_proj_ctx.get(), 1, &path);
    m_transf.reset(proj_create(m_proj_ctx.get(), "+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +R=6378137 +units=m +no_defs"));
//    m_transf.reset(proj_create(m_proj_ctx.get(), "+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +R=1 +units=m +no_defs"));
//    m_transf.reset(proj_create_crs_to_crs(m_proj_ctx.get(), "EPSG:4326", "+proj=merc +lon_0=0 +k=1 +x_0=0 +y_0=0 +ellps=WGS84 +datum=WGS84 +units=m +no_defs", nullptr));
    if(!m_transf) {
        auto err = proj_context_errno(m_proj_ctx.get());
        throw std::runtime_error(proj_errno_string(err));
    }
}

Vertex2m GeoConverterProj::geoToPlain(const Vertex2d geo_crd) {

    auto a = proj_coord(geo_crd.x, geo_crd.y, 0, 0);
    PJ_COORD b = proj_trans(m_transf.get(), PJ_FWD, a);
    return {units::meter{b.xy.x}, units::meter{b.xy.y}};
}

Vertex2d GeoConverterProj::plainToGeo(const Vertex2m p) {

    auto a = proj_coord(p.x.value(), p.y.value(), 0, 0);
    PJ_COORD b = proj_trans(m_transf.get(), PJ_INV, a);
    return {b.enu.e, b.enu.n};

}

units::meter GeoConverterProj::CorrectPlainX(units::meter x) {

    double cnt = x/PLAIN_X_LEN;
    if(x < 0)
        cnt = ceil(cnt);
    else
        cnt = floor(cnt);
    x = x - PLAIN_X_LEN*cnt;

    if(x >= PLAIN_X_MAX)
        x = x - PLAIN_X_LEN;
    else if(x < PLAIN_X_MIN)
        x = x + PLAIN_X_LEN;

    return x;
}

} // namespace geomap {
